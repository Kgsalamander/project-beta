from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO


class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = ["employee_id",
                  "first_name",
                  "last_name",
                  "id"]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name",
                  "last_name",
                  "employee_id",
                  "id"]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name",
                  "last_name",
                  "phone_number",
                  "address",
                  "id"]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name",
                  "last_name",
                  "address",
                  "phone_number",
                  "id"]


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin",
                  "sold",
                  "id"]


class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = ["price",
                  "automobile",
                  "customer",
                  "salesperson",
                  "id"
                  ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "customer": CustomerListEncoder(),
        "salesperson": SalespersonListEncoder()
    }
    def get_extra_data(self, o):
        return {"automobile": o.automobile.vin}


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def api_show_salespeople(request, id):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(employee_id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False
        )
    else:
        count, _ = Salesperson.objects.filter(employee_id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def api_show_customers(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False
        )
    else:
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleDetailEncoder,
            safe=False
        )

    else:
        try:
            content = json.loads(request.body)

            vin = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = vin

            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            content["salesperson"] = salesperson

            customer = Customer.objects.get(phone_number=content["customer"])
            content["customer"] = customer

            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleDetailEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Vin Number"},
                status=400
            )


@require_http_methods(["GET", "DELETE"])
def api_show_sale(request, id):
    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False
        )
    else:
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse(
            {"delete": count > 0}
        )
