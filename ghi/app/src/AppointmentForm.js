import React, {useEffect, useState} from "react"


function AppointmentForm() {
    const [technicians, setTechs] = useState([])
    const [technician, setTech] = useState('')
    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [reason, setReason] = useState('')

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTech(value);
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }
    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const body = {
            vin: vin,
            status: "in Progress",
            customer: customer,
            reason: reason,
            date_time: `${date} ${time}`,
            technician: technician,
        }

        const postUrl = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(postUrl, fetchConfig);
        if (response.ok) {
            setVin('')
            setCustomer('')
            setDate('')
            setTime('')
            setReason('')
            setTech('')
        }

    }

    const fetchData = async() => {
        const Url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(Url)
            if (response.ok) {
            const data = await response.json()
            setTechs(data.technicians)
            }
    }
    useEffect(() => {fetchData();}, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center">New Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="vin" required type="vin" name ="vin" id="vin" className="form-control"/>
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange} value={customer} placeholder="customer" required type="customer" name="customer" id="customer" className="form-control"/>
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} value={reason} placeholder="reason" required type="reason" name="reason" id="reason" className="form-control"/>
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateChange} value={date} required type="date" name="date" id="date" className="form-control"/>
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTimeChange} value={time} required type="time" name="time" id="time" className="form-control"/>
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleTechnicianChange} value={technician} required type="technician" id="technician" className="form-select">
                                <option value="">Technician</option>
                                {technicians.map((technician) => {
                                    return (
                                    <option key={technician.id} value={technician.id}>
                                        {technician.first_name + " " + technician.last_name}
                                    </option>
                                    )
                                    })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create Appointment</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default AppointmentForm
