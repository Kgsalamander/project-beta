import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonForm from './SalespersonForm';
import SalespersonList from './SalespersonList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import ModelForm from './ModelForm';
import ModelList from './ModelList';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import TechList from './TechList';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import TechForm from './TechForm';
import AppointmentForm from './AppointmentForm';
import SalespersonHistory from './SalespersonHistory';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';


function App({salespeople}) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='techs'>
            <Route index element={<TechList />}/>
          </Route>
          <Route path='techs'>
            <Route path='new' element={<TechForm />}/>
          </Route>
          <Route path='appointments'>
            <Route index element={<AppointmentList />}/>
          </Route>
          <Route path='appointments'>
            <Route path='new' element={<AppointmentForm />}/>
          </Route>
          <Route path='servicehistory'>
            <Route index element={<ServiceHistory />}/>
          </Route>
          <Route path="salespeople">
            <Route path="new" element={<SalespersonForm />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespersonList salespeople={salespeople} />} />
          </Route>
          <Route path="customers">
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
          </Route>
          <Route path="manufacturers">
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
          </Route>
          <Route path="models">
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelList />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList />} />
          </Route>
          <Route path="sales">
            <Route path="new" element={<SalesForm />} />
          </Route>
          <Route path="salespersonhistory">
            <Route index element={<SalespersonHistory />} />
          </Route>
          <Route path="automobiles">
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
