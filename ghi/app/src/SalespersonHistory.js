import React, { useState, useEffect } from 'react';


function SalespersonHistory () {

    const [salespersons, setSalespersons] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [sales, setSales] = useState([]);
    const [selectedSales, setSelected] = useState('')

    const fetchSalespersonData = async () => {
        const SalespersonUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(SalespersonUrl);
        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespeople)
        }
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleSelected = event => setSelected(event.target.value)
    
    useEffect( () => {fetchSalespersonData()}, [])

    const fetchSalesData = async () => {
    const SalesUrl = 'http://localhost:8090/api/sales/';
    const response = await fetch(SalesUrl);
    if (response.ok) {
        const data = await response.json();
        setSales(data.sales)
        }
    }

   useEffect( () => {fetchSalesData()}, [])

    return (
    <div>
        <h2>Salesperson History</h2>
        <div className="form-floating mb-3">
                <select onChange={handleSelected} value={selectedSales} placeholder="Salesperson" required name="salesperson" id="salesperson" className="form-control">
                <option value="">Choose a salesperson</option>
                {salespersons.map(saleppl => {
                    return (
                        <option key={saleppl.employee_id} value={saleppl.id}>
                            {saleppl.first_name} {saleppl.last_name}
                        </option>
                    );
                    })}
                </select>
                </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Salesperson</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
                {sales.filter(sale => selectedSales)
                .map((sale) => {
                        return (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
    );
};
export default SalespersonHistory;
